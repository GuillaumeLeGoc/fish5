function [] = displaySignal(this, varargin)
% FisH5.DISPLAYSIGNAL
% FisH5.DISPLAYSIGNAL('data1', 'data2')
% FisH5.DISPLAYSIGNAL(_, 'back', 'seg')
% FisH5.DISPLAYSIGNAL(_, 'range', [min max])
% FisH5.DISPLAYSIGNAL(_, 'load', 'all')
% Display a background image on which the uer can click to see the temporal
% of the selected neuron.
%
% INPUTS :
% ------
% data : choose two or less:  'dff' (default), 'fluo', 'baseline', 'spikes' 
% or 'spikesbin'.
% 'back' Value : either 'seg' (default) or 'mean'. Defines the background
% image.
% 'range', Value : colormap range, default is 'default' (hardcoded).
% 'load', Value : 'all' or 'ondemand' (default), load all data or load 
% specific point on demand.

% --- Check version
if ~this.checkVersion(1)
    warning('File version not supported.');
    return
end

% --- Check input
p = inputParser;
p.addOptional('data1', 'dff', @(x) ischar(x)||isstring(x));
p.addOptional('data2', 'spikes', @(x) ischar(x)||isstring(x));
p.addParameter('back', 'mean', @(x) ischar(x)||isstring(x));
p.addParameter('range', 'default', @(x) ischar(x)||isstring(x)||isnumeric);
p.addParameter('load', 'ondemand', @(x) ischar(x)||isstring(x));
p.parse(varargin{:});

data1 = p.Results.data1;
data2 = p.Results.data2;
background = p.Results.back;
crange = p.Results.range;
ram = p.Results.load;

if ischar(crange)
    if strcmp(crange, 'default') && contains(background, 'seg', 'IgnoreCase', true)
        crange = [0 1];
    elseif strcmp(crange, 'default')
        crange = [400 800];
    end
end

z = 1;

% --- Get data
segmask = this.load('segmentation');
if contains(background, 'seg', 'IgnoreCase', true)
    background = segmask;
else
    background = this.load(background);
end

fprintf('Building pixels list...'); tic
pixels = this.getPixelList(1:this.nneurons);
fprintf('\tDone (%2.2fs).\n', toc);

time = this.load('time');

switch ram
    case 'all'
        values1 = this.load(data1);
        values2 = this.load(data2);
        getValue1 = @(n) values1(n, :);
        getValue2 = @(n) values2(n, :);
    case 'ondemand'
        getValue1 = @(n) this.load(data1, 'n', n);
        getValue2 = @(n) this.load(data2, 'n', n);
end

% --- Create first stack
img = background(:, :, z);

% --- Create figure
fig = figure('Visible', 'off');
% Image
subplot(2, 2, [1 3]);
hi = imshow(img);
hi.ButtonDownFcn = @clickCallback;
caxis(crange);
title(hi.Parent, [this.fishid ', layer ' num2str(z)]);

% Traces
subplot(2, 2, 2);
hp1 = plot(time, NaN(this.ntimes, 1));
hp1.Parent.XLim = [0 time(end)];
xlabel('Time [s]');
ylabel(data1);

subplot(2, 2, 4);
hp2 = plot(time, NaN(this.ntimes, 1));
hp2.Parent.XLim = [0 time(end)];
xlabel('Time [s]');
ylabel(data2);

linkaxes([hp1.Parent, hp2.Parent], 'x');

% --- Create z slider
hz = uicontrol('Style', 'slider', ...
    'Min', 1, 'Max', this.nlayers, ...
    'SliderStep', [1/(this.nlayers-1) 1/(this.nlayers-1)], ...
    'Value', 1, ...
    'Position', [120 20 200 20], ...
    'Callback', @updateZ);

hlz = addlistener(hz, 'ContinuousValueChange', @updateZ);
setappdata(hz, 'sliderListener', hlz);

fig.Visible = 'on';

% -------------------------------------------------------------------------

    function updateZ(source, ~)
        % Updates the image to be displayed.
        z = floor(source.Value);
        img = background(:, :, z);
        set(hi, 'Cdata', img);
        caxis(crange);
        title(hi.Parent, [this.fishid ', layer ' num2str(z)]);
        drawnow;
    end

    function clickCallback(source, ~)
        % Updates the figure given the neurons the user clicked on.
        
        % Get click coordinates
        ax = source.Parent;
        c = round(ax.CurrentPoint(1, 1:2));
        neuron_id = segmask(c(2), c(1), z);
        
        % Fill image
        img = background(:, :, z);
        if neuron_id ~= 0
            img(pixels{neuron_id}) = Inf;
        end
        set(hi, 'Cdata', img);
        caxis(crange);
        title(hi.Parent, [this.fishid ', layer ' num2str(z)]);
        drawnow;
        
        % Update traces
        if neuron_id ~= 0
            hp1.YData = getValue1(neuron_id);
            hp2.YData = getValue2(neuron_id);
            title(hp1.Parent, ['Neuron #' num2str(neuron_id)]);
        end
    end
end