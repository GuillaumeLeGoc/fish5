function h = displayTrace(this, neurons_id, varargin)
% FisH5.DISPLAYTRACE(neurons_id)
% FisH5.DISPLAYTRACE(neuron_id, data)
% FiSH5.DISPLAYTARCE(_, 't', 1:1000)
% h = FisH5.DISPLAYTRACE(_)
% Displays the temporal trace of input neuron(s).
%
% INPUTS :
% ------
% neurons_id : one or several neuron id(s)
% data (optional) : data to plot, either 'dff' (default), 'fluo', 
% 'baseline', 'spikes', 'spikesbin'.
% 't', Value : vector of time steps to display.
%
% RETURNS :
% -------
% h : handle to lines.

% --- Check input
p = inputParser;
p.addRequired('neurons_id', @isnumeric);
p.addOptional('data', 'dff', @(x) ischar(x)||isstring(x));
p.addParameter('t', '', @(x) ischar(x)||isstring(x)||isnumeric(x));
p.parse(neurons_id, varargin{:});
data = p.Results.data;
t = p.Results.t;

% --- Get data
time = this.load('time', 't', t);
delays = this.load('delays', 'n', neurons_id);
values = this.load(data, 'n', neurons_id, 't', t);

% --- Align data
if ~contains(data, 'spikes', 'IgnoreCase', true)
    val_align = NaN(size(values));
    for n = 1:size(values, 1)
        val_align(n, :) = interp1(time+delays(n), single(values(n, :)), time, 'pchip', 'extrap');
    end
else
    val_align = values;     % spikes are already time aligned.
end

% --- Create figure
fig = figure('Visible', 'off');
h = plot(time, val_align);

% --- Style
for i = 1:numel(h)
    h(i).LineWidth = 1.25;
end
grid on
xlabel('Time [s]');
ylabel(data);

fig.Visible = 'on';
end