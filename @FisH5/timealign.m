function signal_aligned = timealign(this, time, signal, neurons_id, varargin)
% signal_aligned = FisH5.TIMEALIGN(time, signal, neurons_id).
% Align the data contained in signal with delays corresponding to
% neurons_id by interpolating values on the same time vector.
%
% INPUTS :
% ------
% time : 1D reference time vector on which the alignment is done.
% signal : 2D array with time series on ROWS to interpolate.
% neurons_id : corresponding neurons_id.
% 'gpu', Value : boolean, perform the interpolation on the GPU if possible.
% Default is false).
% 'method', Value : method for interpolation, check interp1 for possible
% methods. Default is 'pchip'.
%
% RETURNS :
% -------
% signal_aligned : 2D array with time aligned data.

% --- Check input
p = inputParser;
p.addRequired('time', @isnumeric);
p.addRequired('signal', @isnumeric);
p.addRequired('neurons_id', @isnumeric);
p.addParameter('gpu', false, @islogical);
p.addParameter('method', 'pchip', @(x) ischar(x)||isstring(x));
p.parse(time, signal, neurons_id, varargin{:});

time = p.Results.time;
signal = p.Results.signal;
neurons_id = p.Results.neurons_id;
gpu = p.Results.gpu;
method = p.Results.method;

% --- Align data
delays = this.load('delays', 'n', neurons_id);

if gpu && ismember(method, {'linear', 'nearest', 'v5cubic', 'spline'})
    signal = gpuArray(signal);
end

signal_aligned = NaN(size(signal));
for n = 1:size(signal, 1)
    signal_aligned(n, :) = gather(interp1(time+delays(n), single(signal(n, :)), time, method, 'extrap'));
end