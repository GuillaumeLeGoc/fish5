function [] = displayMeanStack(this, varargin)
% FisH5.DISPLAYMEANSTACK()
% FisH5.DISPLAYMEANSTACK('range', [min max])
% Display the temporal mean with a z slider.
%
% INPUTS :
% ------
% 'range', Value (optional) : color axis limit, 'auto' or 'default'
% (default, hardcoded limits).

% --- Check input
p = inputParser;
p.addParameter('range', 'default', @(x) ischar(x)||isstring(x)||isnumeric(x));
p.parse(varargin{:});
crange = p.Results.range;

if ischar(crange)
    if strcmp(crange, 'default')
        crange = [400 800];
    end
end

z = 1;

% --- Get data
meanimage = this.load('temporalmean');
img = meanimage(:, :, z);

% --- Create figure
fig = figure('Visible', 'off');
h = imshow(img);
caxis(crange);
title([this.fishid ', layer ' num2str(z)]);

% --- Create z slider
hz = uicontrol('Style', 'slider', ...
    'Min', 1, 'Max', this.nlayers, ...
    'SliderStep', [1/(this.nlayers-1) 1/(this.nlayers-1)], ...
    'Value', z, ...
    'Position', [260 20 260 20], ...
    'Callback', @updateZ);

hlz = addlistener(hz, 'ContinuousValueChange', @updateZ);
setappdata(hz, 'sliderListener', hlz);

fig.Visible = 'on';

% -------------------------------------------------------------------------

    function updateZ(source, ~)
        % Updates the image to be displayed.
        z = floor(source.Value);
        img = meanimage(:, :, z);
        set(h, 'Cdata', img);
        caxis(crange);
        title([this.fishid ', layer ' num2str(z)]);
        drawnow;
    end

end