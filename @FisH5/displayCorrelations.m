function [] = displayCorrelations(this, data, varargin)
% FisH5.DISPLAYCORRELATIONS
% FiSH5.DISPLAYCORRELATIONS(data)
% Display temporal mean where the user can click on a neuron to display
% correlations with other neurons.
%
% INPUTS :
% ------
% data : char, either 'dff', 'fluo', 'baseline', 'spikes' or 'spikes bin'.
% 'range', Value : 2 elements vector, 'default' (default). FAKE, to be
% fixed.

% --- Check input
p = inputParser;
p.addRequired('data', @(x) ischar(x)||isstring(x));
p.addParameter('range', 'default', @(x) ischar(x)||isstring(x)||isnumeric(x));
p.parse(data);
data = p.Results.data;
crange = p.Results.range;

if ischar(crange)
    if strcmp(crange, 'default')
        crange = [400 800];
    end
end

z = 1;
neuron_id = 0;

% --- Get data
neurons = 1:this.nneurons;
background = this.load('mean');
segmask = this.load('segmentation');
fprintf('Building pixels list...'); tic
[pixels, layers, id_onlayer] = this.getPixelList(neurons);
fprintf('\tDone (%2.2fs).\n', toc);

% --- Compute correlations for each layer
fprintf('Building correlations matrices...'); tic
slices = unique(layers);
correlations = cell(numel(layers), 1);
for lay = slices
    correlations{lay} = this.correlationMatrix(data, 'n', neurons(layers == lay));
end
fprintf('\t Done (%2.2fs).\n', toc);

% --- Create first stack
img = rescale(background(:, :, z), 0, 0.5);
img = cat(3, img, img, img);

% --- Create figure
fig = figure('Visible', 'off');
hi = imshow(img);
hi.ButtonDownFcn = @clickCallback;
caxis(crange);
title(hi.Parent, [this.fishid ', layer ' num2str(z)]);

% --- Create z slider
hz = uicontrol('Style', 'slider', ...
    'Min', 1, 'Max', this.nlayers, ...
    'SliderStep', [1/(this.nlayers-1) 1/(this.nlayers-1)], ...
    'Value', 1, ...
    'Position', [260 20 260 20], ...
    'Callback', @updateZ);

hlz = addlistener(hz, 'ContinuousValueChange', @updateZ);
setappdata(hz, 'sliderListener', hlz);

fig.Visible = 'on';

    function updateZ(source, ~)
        % Updates the image to be displayed.
        z = floor(source.Value);
        img = rescale(background(:, :, z), 0, 0.5);
        img = cat(3, img, img, img);
        set(hi, 'Cdata', img);
        title(hi.Parent, [this.fishid ', layer ' num2str(z)]);
        drawnow;
    end

    function clickCallback(source, ~)
        % Updates the figure given the neurons the user clicked on.
        
        % Get click coordinates
        ax = source.Parent;
        c = round(ax.CurrentPoint(1, 1:2));
        neuron_id = segmask(c(2), c(1), z);
        
        % Fill image
        img = rescale(background(:, :, z), 0, 0.5);
        if neuron_id ~= 0
            values = correlations{z}(id_onlayer(neuron_id), :);
            img = buildrgb(img, pixels(layers == z), values);
        else
            img = cat(3, img, img, img);
        end
        set(hi, 'Cdata', img);
        title(hi.Parent, [this.fishid ', layer ' num2str(z)]);
        drawnow;
        
    end
end

function imgfilled = buildrgb(imgin, pixels, values)
% Fills pixels in in imging with values and build RGB image.
redchan = zeros(size(imgin));
bluchan = zeros(size(imgin));
for id = 1:numel(pixels)
    if values(id) > 0
        redchan(pixels{id}) = values(id);
    elseif values(id) < 0
        bluchan(pixels{id}) = -values(id);
    end
end

imgfilled = cat(3, redchan + imgin, imgin, bluchan + imgin);     
end