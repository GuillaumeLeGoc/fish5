function regions_inds = findRegions(this, neurons_inds)
% regions_inds = H.FINDREGIONS(neurons_inds);
%
% Returns ZBrain regions ID (1-294) to which belong neurons defined by
% neurons_inds.
%
% INPUT :
% -----
% neurons_inds : 1D vector specifying neurons indices to find
%
% RETURNS :
% -------
% regions_inds : cell determining to which regions each neuron belongs to.

% --- Check input
p = inputParser;
p.addRequired('neurons_inds', @isnumeric);
p.parse(neurons_inds);
neurons_inds = p.Results.neurons_inds;

nneurons = numel(neurons_inds);

% --- Get labels array for those neurons
regions = this.load('labels', 'n', neurons_inds);

% --- Init. output cell
regions_inds = cell(nneurons, 1);

% --- Processing
for id = 1:nneurons
    regions_inds{id} = find(regions(id, :));
end