function motion_index = getMotions(this, varargin)
% motion_index = FisH5.GETMOTIONS
% motion_index = FisH5.GETMOTIONS(threshold)
% motion_index = FisH5.GETMOTIONS(_, 'mode', 'abs')
% motion_index = FisH5.GETMOTIONS(_, 'window', window)
% motion_index = FisH5.GETMOTIONS(_, 'dir', 'after')
% motion_index = FisH5.GETMOTIONS(_, 'disp', true)
% Finds time steps where a motion occured based on drift speed. If 'mode'
% is set to 'rel', automatically threshold the speed, if 'mode' is set to
% 'abs', frames where speed is above threshold are discarded.
%
% INPUTS (all optional) :
% ------
% threshold : percentile if 'mode' is set to 'rel' or absolute threshold 
% if 'mode' is set to 'abs'.
% 'mode', Value : 'rel' (default) or 'abs'.
% 'window', Value : number of frames to discard after a motion (default is
% 10).
% 'dir', Value : 'both' (discard frames before and after motion), 'prev'
% (discard frames before the motion) or 'after' (discard frames after the
% motion). Default is 'both'.
% 'disp', Value : true or false, display resulting motion detection.
%
% RETURNS :
% -------
% motion_index : vector containing frames index where motion occured.

% --- Check input
p = inputParser;
p.addOptional('threshold', [], @isnumeric);
p.addParameter('mode', 'rel', @(x) ischar(x)||isstring(x));
p.addParameter('window', 10, @isnumeric);
p.addParameter('dir', 'both', @(x) ischar(x)||isstring(x));
p.addParameter('disp', false, @islogical);
p.parse(varargin{:});

threshold = p.Results.threshold;
mode = p.Results.mode;
window = p.Results.window;
direction = p.Results.dir;
displ = p.Results.disp;

if strcmp(mode, 'rel') && isempty(threshold)
    threshold = 99;
else
    threshold = 1;
end

% --- Get data
drifts= this.load('drift');

% --- Processing
dx = diff(drifts(1, :));
dy = diff(drifts(2, :));
s = dx.^2 + dy.^2;

switch mode
    case 'rel'
        motion_index = isoutlier(s, 'percentiles', [0 threshold]);
    case 'abs'
        motions_init = s > threshold.^2;
        motion_index = zeros(1, this.ntimes);
        motion_index(motions_init) = 1;
end

switch direction
    case 'both'
        SE = ones(1, 2*window + 1);
    case 'after'
        SE = ones(1, 2*window + 1);
        SE(1:window-1) = 0;
    case 'before'
        SE = ones(1, 2*window + 1);
        SE(window:end) = 0;
end

motions_logic = logical(imdilate(motion_index, SE));
motion_index = find(motions_logic);

% --- Display
if displ
    colors = getColors(2);
    okx = drifts(1, :);
    okx(motions_logic) = NaN;
    oky = drifts(2, :);
    oky(motions_logic) = NaN;
    oks = sqrt(s);
    oks(motions_logic) = NaN;
    
    nox = drifts(1, :);
    nox(~motions_logic) = NaN;
    noy = drifts(2, :);
    noy(~motions_logic) = NaN;
    nos = sqrt(s);
    nos(~motions_logic) = NaN;
    
    figure; hold on
    px = plot(okx, 'DisplayName', 'X', 'Color', colors(1, :));
    py = plot(oky, 'DisplayName', 'Y', 'Color', colors(2, :));
    ps = plot(oks, 'DisplayName', 'Speed', 'Color', [0 0 0]);
    plot(nox, 'Color', [1 0 0]);
    plot(noy, 'Color', [1 0 0]);
    pd = plot(nos, 'Color', [1 0 0], 'DisplayName', 'Discarded');
    xlabel('Frames [#]');
    ylabel('Drifts [µm]');
    legend([px, py, ps, pd]);
end