function neurons_inds = findNeuronsInRegion(this, regionid)
% neurons_inds = FisH5.FINDNEURONSINREGIONS(regionid);
% Finds neurons indices that are in the brain region specified by regionid.
%
% INPUTS :
% ------
% regionid : ids of the region. See the ZBrain Atlas regions file. If
% empty, all neurons are returned.
%
% RETURNS :
% -------
% neurons_inds : neurons indices member of the input regions.

% --- Check input
p = inputParser;
p.addRequired('regionid', @isnumeric);
p.parse(regionid);

regionid = p.Results.regionid;

if isempty(regionid)
    neurons_inds = 1:this.nneurons;
    return
end

% --- Get data
labels = this.load('labels');

% --- Processing
labid = 1:size(labels, 2);
labels(:, ~ismember(labid, regionid)) = [];
[neurons_inds, ~] = find(labels);