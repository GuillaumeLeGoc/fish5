function out = findDataset(this, datasetname, varargin)
% FisH5.FINDDATASET(datasetname)
% FisH5.FINDATASET(_, 'warn', 'off')
% Look for datasetname in all the datasets of the h5 file. 
% If several datasets have the same name, returns a cell with the different
% names.
%
% INPUT :
% -----
% datasetname : char, name if the dataset to find.
% 'warn', Value (optional) : boolean, turn on (default) or off warnings.
%
% RETURN :
% ------
% out : char, full path to the dataset within the h5 file.

% --- Check input
p = inputParser;
p.addRequired('datasetname', @(x) ischar(x)||isstring(x));
p.addParameter('warn', true, @islogical);
p.parse(datasetname, varargin{:});
datasetname = p.Results.datasetname;
warnings = p.Results.warn;

% --- Scan
groups = this.fileinfo;
append = '';
out = cell(0, 1);
while true
    
    groups = eval(['cat(1, groups(:)' append ')']);     % To check also root
    if isempty(groups)
        break;
    end
    
    datasets = cat(1, groups(:).Datasets);              % Get all datasets
    
    names = cell(0, 1);
    for g = 1:length(groups)
        names = cat(1, names, cellstr(repmat(groups(g).Name, length(groups(g).Datasets), 1)));
    end
    
    for id = 1:length(datasets)
        if strcmpi(datasets(id).Name, datasetname)
            out{end + 1} = strjoin([names(id) '/' datasets(id).Name], '');
        end
    end    
    
    append = '.Groups';
    
end

if isempty(out)
    out = [];
    if warnings
        warning('No dataset found.');
    end
elseif length(out) > 1
    if warnings
        warning([num2str(length(out)) ' datasets found.']);
    end
else
    out = strjoin(out, '');
end