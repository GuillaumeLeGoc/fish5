function out = load(this, datasetname, varargin)
% FisH5.LOAD(dataset) : Load specified dataset as a keyword.
% FisH5.LOAD(_, 'force', true) : Try to find and load dataset.
% FisH5.LOAD(_, 'n', neurons_id) : Loads only data for neurons_id.
% FisH5.LOAD(_, 't', times_id) : Loads only data for times_id.
%
% INPUT :
% -----
% datasetname : char. Can be one of the following :
%   - full path to dataset within the h5 file.
%   - keyword. If the keyword is not understood, the data is searched
%   through all datasets within the file.
% 'force', Value (optional) : boolean, force the search and don't use
% keyword. Default = false.
% 'n', Value : neurons indices to load.
% 't', Value : time steps to load.
%
% RETURNS :
% -------
% out : the required dataset.

% --- Check input
p = inputParser;
p.addRequired('datasetname', @(x) ischar(x)||isstring(x));
p.addParameter('force', false, @islogical);
p.addParameter('n', []);
p.addParameter('t', []);
p.parse(datasetname, varargin{:});

datasetname = p.Results.datasetname;
forcefind = p.Results.force;
n = p.Results.n;
t = p.Results.t;

% --- Find dataset
if ~forcefind
    
    % --- Try input as the full path to the dataset
    if ~strcmp(datasetname(1), '/')
        datasetname = ['/' datasetname];
    end
    try
        out = this.h5read(datasetname, n, t);
        return
    catch
        datasetname(1) = [];
    end
    
    % --- Try input as a known keyword
    switch lower(datasetname)
        
        case {'coordinates', 'coord', 'coords'}
            dataset = '/Data/Brain/Coordinates';
        case {'ljpcoordinates', 'labcoord', 'labcoords'}
            dataset = '/Data/Brain/LJPCoordinates';
        case {'labels', 'lab'}
            dataset = '/Data/Brain/Labels';
        case {'rawsignal', 'fluo', 'signal', 'sig'}
            dataset = '/Data/Brain/RawSignal';
        case {'timedelays',  'lags', 'lag', 'delay', 'delays'}
            dataset = '/Data/Brain/TimeDelays';
        case {'times', 't', 'time'}
            dataset = '/Data/Brain/Times';
        case {'zbraincoordinates', 'refcoords', 'refcoord', 'zbraincoord', 'zbraincoords'}
            dataset = '/Data/Brain/ZBrainCoordinates';
        case {'drift', 'drifts'}
            dataset = '/Data/Brain/Analysis/Drifts';
        case {'baseline', 'bl', 'base'}
            dataset = '/Data/Brain/Analysis/Baseline';
        case {'dff', 'df/f', 'df'}
            dataset = '/Data/Brain/Analysis/DFF';
        case {'inferredspikes', 'spikes'}
            dataset = '/Data/Brain/Analysis/InferredSpikes';
        case {'thresholdedspikes', 'binspikes', 'spikesbin'}
            dataset = '/Data/Brain/Analysis/ThresholdedSpikes';
        case {'segmentation', 'segmask', 'seg'}
            dataset = '/Data/Brain/Pixels/Segmentation';
        case {'mean', 'temporalmean', 'greystack', 'grey_stack'}
            dataset = '/Data/Brain/Pixels/TemporalMean';
            
        otherwise
            % Look for the dataset name within the file.
            dataset = this.findDataset(datasetname, 'warn', false);
    end
    
    % Try to load the dataset
    try
        out = this.h5read(dataset, n, t);
        return
    catch
    end
end

% --- Last attempt to find dataset name
dataset = this.findDataset(datasetname, 'warn', false);
try
    out = this.h5read(dataset, n, t);
    return
catch
    warning('Dataset not found.');
    out = [];
end