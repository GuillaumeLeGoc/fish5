classdef FisH5
    % FisH5 is the class to handle LJP calcium imaging HDF5 files.
    
    % ---------------------------------------------------------------------
    
    properties
        
        fileinfo
        date
        run
        fishline
        fishage
        fishid
        framerate
        nlayers
        nneurons
        ntimes
        dx
        dz
        timestamp
        fileversion
        filename
        
    end
    
    % ---------------------------------------------------------------------
    
    methods
        
        function this = FisH5(filename)
            % Constructor. Gets metadata from file.
            
            if ~exist(filename, 'file')
                error('File not found.');
            end
            
            this.filename = filename;
            this.fileinfo = h5info(filename);
            this.date = this.findAttribute('date', 'warn', false);
            this.run = this.checkdouble(this.findAttribute('run', 'warn', false));
            this.fishline = this.findAttribute('line', 'warn', false);
            this.fishage = this.checkdouble(this.findAttribute('age', 'warn', false));
            this.fishid = this.findAttribute('fish id', 'warn', false);
            this.framerate = this.checkdouble(this.findAttribute('rate', 'warn', false));
            this.nlayers = this.checkdouble(this.findAttribute('layers', 'warn', false));
            this.nneurons = this.findNumberNeurons;
            this.ntimes = this.findNumberTimes;
            this.dx = this.checkdouble(this.findAttribute('pixel', 'warn', false));
            this.dz = this.checkdouble(this.findAttribute('increment', 'warn', false));
            this.timestamp = this.findAttribute('creation', 'warn', false);
            this.fileversion = this.checkchar(this.findAttribute('version', 'warn', false));
            
        end
        
    end
    
    % ---------------------------------------------------------------------
    
    methods (Hidden = true)
        
        function out = checkdouble(~, in)
            % Check if the input is double and convert it if not.
            
            if ~isnumeric(in)
                out = str2double(in);
            else
                out = in;
            end
            
        end
        
        % ---
        
        function out = checkchar(~, in)
            % Check if input is char and convert it if not.
            
            if isnumeric(in)
                out = num2str(in);
            elseif isstring(in)
                out = char(in);
            else
                out = in;
            end
            
        end
        
        % ---
        
        function out = findNumberNeurons(this)
            % Try to find number of neurons.
            
            try
                out = length(this.load('coordinates'));
            catch
                out = [];
            end
            
        end
        
        % ---
        
        function out = findNumberTimes(this)
            % Try to find number of time steps.
            
            try
                tmp = size(this.load('fluo'));
                out = tmp(tmp ~= this.nneurons);
            catch
                out = [];
            end
            
        end
        
        function out = checkVersion(this, minimum)
            % Check if the file version is at least minimum. Returns true
            % if it is the case, false otherwise.
            
            version = this.fileversion;
            version(regexp(version, '[a-zA-Z]')) = [];
            version = str2double(version);
            out = version >= minimum;
        end
        
    end
end