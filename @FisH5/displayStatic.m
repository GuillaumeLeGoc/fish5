function [] = displayStatic(this, data, varargin)
% FisH5.DISPLAYSTATIC(data);
% FisH5.DISPLAYSTATIC(data, 'range', [0, 1]);
% Display temporal mean overlaid with input data. Negative data is set to
% blue, positive data to red.
%
% INPUTS :
% ------
% data : data to overlay, nneurons x 1.
% 'range', [min max] : (optional), specify data range

% --- Check input
p = inputParser;
p.addRequired('data', @isnumeric);
p.addParameter('range', 'auto', @(x) ischar(x)||isstring(x)||isnumeric(x));
p.parse(data);
data = p.Results.data;
crange = p.Results.range;

if numel(data) ~= this.nneurons
    error('Input data doesn''t match neurons number.');
end

if ischar(crange)
    switch crange
        case 'default'
            crange = [0, 1];
        case 'auto'
            crange = [min(data), max(data)];
    end
end

z = 1;
% neuron_id = 0;

% --- Get data
neurons = 1:this.nneurons;
background = this.load('mean');
fprintf('Building pixels list...'); tic
[pixels, layers] = this.getPixelList(neurons);
fprintf('\tDone (%2.2fs).\n', toc);

% --- Build data matrix
fprintf('Building data matrices...'); tic
% Apply range
data(data < crange(1)) = crange(1);
data(data > crange(2)) = crange(2);
% Rescale data to build RGB image
data(data < 0) = rescale(data(data < 0), -0.5, 0);  % rescale neg. data
data(data > 0) = rescale(data(data > 0), 0, 0.5);   % rescale pos. data

% Build matrices per layer
slices = unique(layers);
dispdata = cell(numel(layers), 1);
for lay = slices
    dispdata{lay} = data(neurons(layers == lay));
end
fprintf('\t Done (%2.2fs).\n', toc);

% --- Create first stack
img = rescale(background(:, :, z), 0, 0.5);
values = dispdata{1};
img = buildrgb(img, pixels(layers == 1), values);

% --- Create figure
fig = figure('Visible', 'off');
hi = imshow(img);
caxis(crange);
title(hi.Parent, [this.fishid ', layer ' num2str(z)]);

% --- Create z slider
hz = uicontrol('Style', 'slider', ...
    'Min', 1, 'Max', this.nlayers, ...
    'SliderStep', [1/(this.nlayers-1) 1/(this.nlayers-1)], ...
    'Value', 1, ...
    'Position', [260 20 260 20], ...
    'Callback', @updateZ);

hlz = addlistener(hz, 'ContinuousValueChange', @updateZ);
setappdata(hz, 'sliderListener', hlz);

fig.Visible = 'on';

    function updateZ(source, ~)
        % Updates the image to be displayed.
        % Get z
        z = floor(source.Value);
        img = rescale(background(:, :, z), 0, 0.5);
        
        % Build RGB image
        values = dispdata{z};
        img = buildrgb(img, pixels(layers == z), values);
        
        set(hi, 'Cdata', img);
        title(hi.Parent, [this.fishid ', layer ' num2str(z)]);
        drawnow;
    end
end

function imgfilled = buildrgb(imgin, pixels, values)
% Fills pixels in in imging with values and build RGB image.
redchan = zeros(size(imgin));
bluchan = zeros(size(imgin));
for id = 1:numel(pixels)
    if values(id) > 0
        redchan(pixels{id}) = values(id);
    elseif values(id) < 0
        bluchan(pixels{id}) = -values(id);
    end
end

imgfilled = cat(3, redchan + imgin, imgin, bluchan + imgin);     
end