function [] = showNeurons(this, neurons_inds, varargin)
% FisH5.SHOWNEURONS(neurons_ind)
% FisH5.SHOWNEURONS(_, 'segmentation')
% Show the temporal mean with sliders and highlights input neurons.
%
% INPUTS :
% ------
% neurons_id : neurons indices. If it is a cell, index in the same cell
% will be colorized with the same color.
% background (optional) : either 'mean' or 'seg', defines the background.
% 'range', Value (optional) : color axis limit, 'auto' or 'default'
% (default, hardcoded limits).

% --- Check input
p = inputParser;
p.addRequired('neurons_ind', @isnumeric);
p.addOptional('back', 'temporalmean', @(x) ischar(x)||isstring(x));
p.addParameter('range', 'default', @(x) ischar(x)||isstring(x)||isnumeric(x));
p.parse(neurons_inds, varargin{:});

neurons_inds = p.Results.neurons_ind;
back = p.Results.back;
crange = p.Results.range;

if ischar(crange)
    if strcmp(crange, 'default') && contains(back, 'seg', 'IgnoreCase', true)
        crange = [0 1];
    elseif strcmp(crange, 'default')
        crange = [400 800];
    end
end

% --- Get data
segmask = this.load('segmentation');
if contains(back, 'seg', 'IgnoreCase', true)
    background = segmask;
else
    background = this.load(back);
end

fprintf('Building pixels list...'); tic
[pixels, layers] = this.getPixelList(neurons_inds);
fprintf('\tDone (%2.2fs).\n', toc);

colors = getColors;
colors = repmat(colors, [ceil(numel(neurons_inds)/size(colors, 1)) 1]);
colors = colors(1:numel(neurons_inds), :);

z = 1;

% --- Create first stack
img = buildrgb(background(:, :, z), pixels(layers == z), colors);

% --- Create figure
fig = figure('Visible', 'off');
h = imshow(img);
caxis(crange);
title([this.fishid ', layer ' num2str(z)]);

% --- Create z slider
hz = uicontrol('Style', 'slider', ...
    'Min', 1, 'Max', this.nlayers, ...
    'SliderStep', [1/(this.nlayers-1) 1/(this.nlayers-1)], ...
    'Value', z, ...
    'Position', [260 20 260 20], ...
    'Callback', @updateZ);

hlz = addlistener(hz, 'ContinuousValueChange', @updateZ);
setappdata(hz, 'sliderListener', hlz);

fig.Visible = 'on';

    function updateZ(source, ~)
        % Updates the image to be displayed.
        z = floor(source.Value);
        img = buildrgb(background(:, :, z), pixels(layers == z), colors);
        set(h, 'Cdata', img);
        caxis(crange);
        title([this.fishid ', layer ' num2str(z)]);
        drawnow;
    end

end

% -------------------------------------------------------------------------

function imgfilled = buildrgb(imgin, pixels, colors)
% Fills pixels in in imging with values and build RGB image.
imgin = rescale(imgin, 0, 1);
redchan = zeros(size(imgin));
grechan = zeros(size(imgin));
bluchan = zeros(size(imgin));
for id = 1:numel(pixels)
    redchan(pixels{id}) = colors(id, 1);
    grechan(pixels{id}) = colors(id, 2);
    bluchan(pixels{id}) = colors(id, 3);
    imgin(pixels{id}) = 0;
end

imgfilled = cat(3, redchan + imgin, grechan + imgin, bluchan + imgin);     
end