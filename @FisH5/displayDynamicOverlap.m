function displayDynamicOverlap(this, movie, data1, data2, varargin)
% FisH5.DISPLAYDYNAMICOVERLAP('movie', 'data1', 'data2')
% FisH5.DISPLAYDYNAMICOVERLAP(_, 'range', [min max])
% FisH5.DISPLAYDYNAMICOVERLAP(_, 'load', 'all')
% FisH5.DISPLAYDYNAMICOVERLAP(_, 'conv', true)
% Viewer of volumetric dynamic data with sliders, with the ability to click
% on neurons and show the trace with a temporal cursor. The two input data
% traces are overlaid on the same graph.
%
% INPUTS :
% ------
% movie : choose which data to display as image.
% data1 : first trace to show.
% data2 : second trace to show.
% 'range', Value : colormap range, default is 'default' (hardcoded).
% 'load', Value : 'all' or 'ondemand' (default), load all data or load 
% specific points on demand.
% 'conv', Value : true or false (default). Convolves second trace with
% GCaMP6 kernel. Requires Inference parameters and spikes.

% --- Check version
if ~this.checkVersion(1)
    warning('File version not supported.');
    return
end

% --- Check input
p = inputParser;
p.addRequired('movie', @(x) ischar(x)||isstring(x));
p.addRequired('data1', @(x) ischar(x)||isstring(x));
p.addRequired('data2', @(x) ischar(x)||isstring(x));
p.addParameter('range', 'default', @(x) ischar(x)||isstring(x)||isnumeric(x));
p.addParameter('load', 'ondemand', @(x) ischar(x)||isstring(x));
p.addParameter('conv', false, @islogical);
p.parse(movie, data1, data2, varargin{:});

movie = p.Results.movie;
data1 = p.Results.data1;
data2 = p.Results.data2;
convol = p.Results.conv;

crange = p.Results.range;
ram = p.Results.load;

if ischar(crange)
    if strcmp(crange, 'default')
        switch movie
            case 'dff'
                crange = [-.25 .5];
            case 'baseline'
                crange = [400 1200];
            case 'fluo'
                crange = [400 1200];
            case 'spikes'
                crange = [0 .35];
            case 'spikesbin'
                crange = [0 1.5];
            otherwise
                crange = 'auto';
        end
    end
end

z = 1;
t = 1;
neuron_id = 0;
layer_id = 1;

% --- Get data
segmask = this.load('segmentation');
[nrows, ncols, ~] = size(segmask);

fprintf('Building pixels list...'); tic
[pixels, layers] = this.getPixelList(1:this.nneurons);
fprintf('\tDone (%2.2fs).\n', toc);time = this.load('time');

switch ram
    case 'all'
        fillvalues = this.load(movie);
        values1 = this.load(data1);
        values2 = this.load(data2);
        
        if convol
            values2 = this.convolve(values2);
        end
        
        getFillValues = @(n, t) fillvalues(n, t);
        getValue1 = @(n) values1(n, :);
        getValue2 = @(n) values2(n, :);
    case 'ondemand'
        
        getFillValues = @(n, t) this.load(movie, 'n', n, 't', t);
        getValue1 = @(n) this.load(data1, 'n', n);
        if convol
            getValue2 = @(n) this.convolve(this.load(data2, 'n', n));
        else
            getValue2 = @(n) this.load(data2, 'n', n);
        end
end

% --- Create first stack
img = zeros(nrows, ncols);
img = fillimage(img, pixels(layers == z), getFillValues(layers == z, t));

% --- Create figure
fig = figure('Visible', 'off');
% Image
subplot(2, 2, [1 3]);
hi = imshow(img);
hi.ButtonDownFcn = @clickCallback;
caxis(crange);
title(hi.Parent, [this.fishid ', layer ' num2str(z) ' frame ' num2str(t)]);

% Traces
subplot(2, 2, [2 4]); hold on
% data 1
hp1 = plot(time, NaN(this.ntimes, 1));
hp1.Parent.XLim = [0 time(end)];
ylabel(data1);
% data 2
hp2 = plot(time, NaN(this.ntimes, 1));
hp2.Parent.XLim = [0 time(end)];
ylabel(data2);
% cursor
yyaxis(hp1.Parent, 'right');
hp1c = plot([t, t], [0, 1]);
hp1c.Parent.YTick = [];
xlabel('Time [s]');

% --- Create z slider
hz = uicontrol('Style', 'slider', ...
    'Min', 1, 'Max', this.nlayers, ...
    'SliderStep', [1/(this.nlayers-1) 1/(this.nlayers-1)], ...
    'Value', 1, ...
    'Position', [120 20 200 20], ...
    'Callback', @updateZ);

hlz = addlistener(hz, 'ContinuousValueChange', @updateZ);
setappdata(hz, 'sliderListener', hlz);

% --- Create t slider
ht = uicontrol('Style', 'slider', ...
    'Min', 1, 'Max', this.ntimes, ...
    'SliderStep', [1/(this.ntimes-1) 1/(this.ntimes-1)], ...
    'Value', t, ...
    'Position', [120 40 200 20], ...
    'Callback', @updateT);

hlt = addlistener(ht,'ContinuousValueChange', @updateT);
setappdata(ht,'sliderListener',hlt);

fig.Visible = 'on';

% -------------------------------------------------------------------------

    function updateZ(source, ~)
        % Updates the z image to be displayed.
        z = floor(source.Value);
        img = zeros(nrows, ncols);
        img = fillimage(img, pixels(layers == z), getFillValues(layers == z, t));
        if layer_id == z && neuron_id ~= 0
            img(pixels{neuron_id}) = Inf;
        end
        set(hi, 'Cdata', img);
        caxis(crange);
        title(hi.Parent, [this.fishid ', layer ' num2str(z) ', frame ', num2str(t)]);
        drawnow;
    end

    function updateT(source, ~)
        % Updates the t image to be displayed.
        t = floor(source.Value);
        img = zeros(nrows, ncols);
        img = fillimage(img, pixels(layers == z), getFillValues(layers == z, t));
        if layer_id == z && neuron_id ~= 0
            img(pixels{neuron_id}) = Inf;
        end
        set(hi, 'Cdata', img);
        caxis(crange);
        title(hi.Parent, [this.fishid ', layer ' num2str(z) ', frame ', num2str(t)]);
        updateCursor(t);
        drawnow;
    end

    function clickCallback(source, ~)
        % Updates the figure given the neurons the user clicked on.
        
        % Get click coordinates
        ax = source.Parent;
        c = round(ax.CurrentPoint(1, 1:2));
        layer_id = z;
        neuron_id = segmask(c(2), c(1), z);
        
        % Update image & traces
        if neuron_id ~= 0
            % image
            img = zeros(nrows, ncols);
            img = fillimage(img, pixels(layers == z), getFillValues(layers == z, t));
            img(pixels{neuron_id}) = Inf;
            set(hi, 'Cdata', img);
            caxis(crange);
            title(hi.Parent, [this.fishid ', layer ' num2str(z) ', frame ', num2str(t)]);
            drawnow;
            % traces
            hp1.YData = getValue1(neuron_id);
            hp2.YData = getValue2(neuron_id);
            hp2.Parent.YLim = hp1.Parent.YLim;
            title(hp1.Parent, ['Neuron #' num2str(neuron_id)]);
            updateCursor(t);
        end
    end

    function updateCursor(t)
        % Draw a cursor at time step t on the 2 subplots.
        hp1c.XData = [time(t) time(t)];
        hp1c.YData = hp1.Parent.YLim;
        hp2c.XData = [time(t) time(t)];
        hp2c.YData = hp1.Parent.YLim;
        
    end
end

% -------------------------------------------------------------------------
function imgfilled = fillimage(imgin, pixels, values)
% Fills pixels in in imging with values.
imgfilled = imgin;
for id = 1:numel(pixels)
    imgfilled(pixels{id}) = values(id);
end
end