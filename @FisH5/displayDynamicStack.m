function [] = displayDynamicStack(this, data, varargin)
% FisH5.DISPLAYDYNAMICSTACK(data)
% FisH5.DISPLAYDYNAMICSTACK(_, 'range', [min max])
% FisH5.DISPLAYDYNAMICSTACK(_, 'load', 'all')
% Simple viewer of dynamic volumetric data with sliders.
%
% INPUTS :
% ------
% data (optional): either 'dff' (default), 'baseline', 'fluo', 'spikes', 
% 'spikesbin'.
% 'range', Value (optional) : color axis limit, 'auto' or 'default' 
% (default, hardcoded limits).
% 'load', Value : 'all' or 'ondemand' (default), load all data or load 
% specific point on demand.

% --- Check input
p = inputParser;
p.addRequired('data', @(x) ischar(x)||isstring(x));
p.addParameter('range', 'default', @(x) ischar(x)||isstring(x));
p.addParameter('load', 'ondemand', @(x) ischar(x)||isstring(x));
p.parse(data, varargin{:});

data = p.Results.data;
crange = p.Results.range;
ram = p.Results.load;

if ischar(crange)||isstring(crange)
    if strcmp(crange, 'default')
    switch data
        case 'dff'
            crange = [-.25 .75];
        case 'baseline'
            crange = [0 600];
        case 'fluo'
            crange = [300 1000];
        case 'spikes'
            crange = [0 .75];
        case 'spikesbin'
            crange = [0 1];
        otherwise
            crange = 'auto';
    end
    end
end

z = 1;
t = 1;

% --- Get data
meanimg = this.load('mean');
[nrows, ncols, ~] = size(meanimg);

fprintf('Building pixels list...'); tic
[pixels, layers] = this.getPixelList(1:this.nneurons);
fprintf('\tDone (%2.2fs).\n', toc);

switch ram
    case 'all'
        fillvalues = this.load(data);
        getFillValues = @(n, t) fillvalues(n, t);
    case 'ondemand'
        getFillValues = @(n, t) this.load(data, 'n', n, 't', t);
end

% --- Create first stack
img = zeros(nrows, ncols);
pixlist = pixels(layers == z);
valueslay = getFillValues(layers == z, t);
img = fillimage(img, pixlist, valueslay);

% --- Create figure
fig = figure('Visible', 'off');
h = imshow(img);
caxis(crange);
title([this.fishid ', layer ' num2str(z) ', t = ', num2str(t)]);

% --- Create z slider
hz = uicontrol('Style', 'slider', ...
    'Min', 1, 'Max', this.nlayers, ...
    'SliderStep', [1/(this.nlayers-1) 1/(this.nlayers-1)], ...
    'Value', z, ...
    'Position', [260 20 260 20], ...
    'Callback', @updateZ);

hlz = addlistener(hz, 'ContinuousValueChange', @updateZ);
setappdata(hz, 'sliderListener', hlz);

% --- Create t slider
ht = uicontrol('Style', 'slider', ...
    'Min', 1, 'Max', this.ntimes, ...
    'SliderStep', [1/(this.ntimes-1) 1/(this.ntimes-1)], ...
    'Value', t, ...
    'Position', [260 40 260 20], ...
    'Callback', @updateT);

hlt = addlistener(ht,'ContinuousValueChange', @updateT);
setappdata(ht,'sliderListener',hlt);

fig.Visible = 'on';

% -------------------------------------------------------------------------

    function updateZ(source, ~)
        % Updates the z image to be displayed.
        z = floor(source.Value);
        img = zeros(nrows, ncols);
        pixlist = pixels(layers == z);
        valueslay = getFillValues(layers == z, t);
        img = fillimage(img, pixlist, valueslay);
        set(h, 'Cdata', img);
        caxis(crange);
        title([this.fishid ', layer ' num2str(z) ', t = ', num2str(t)]);
        drawnow;
    end

    function updateT(source, ~)
        % Updates the t image to be displayed.
        t = floor(source.Value);
        img = zeros(nrows, ncols);
        pixlist = pixels(layers == z);
        valueslay = getFillValues(layers == z, t);
        img = fillimage(img, pixlist, valueslay);
        set(h, 'Cdata', img);
        caxis(crange);
        title([this.fishid ', layer ' num2str(z) ', t = ', num2str(t)]);
        drawnow;
    end
end

% -------------------------------------------------------------------------

function imgfilled = fillimage(imgin, pixels, values)
% Fills pixels in in imging with values.
imgfilled = imgin;
for id = 1:numel(pixels)
    imgfilled(pixels{id}) = values(id);
end
end