# FisH5
Extremely simple class to handle LJP h5 file generated after analysis of calcium imaging data.

## Installation
One of the following :  
* (recommanded) Clone the repository (https://gitlab.com/GuillaumeLeGoc/fish5.git) with `git clone` or with any git client to get updates by pulling. Add the folder containing @FisH5 folder in the MATLAB path.  
* Download the repository ([here](https://gitlab.com/GuillaumeLeGoc/fish5/-/archive/master/fish5-master.zip)) and add the folder to the MATLAB path.  

It is a standalone package, you only need the @FisH5 class and a HDF5 file. It has been tested with MATLAB R2019a but probably works with older versions.

## Usage
`filename = '/path/to/file.h5';`  
`H = FisH5(filename);`  

You can then access several properties using dot notation :
	
	fileinfo
    date
    run
    fishline
    fishage
    fishid
    framerate
    nlayers
    nneurons
    ntimes
    dx
    dz
    timestamp
    fileversion
    filename

### Loading data
You can load some dataset :

`dff = H.load('dff')`

Load only data corresponding to neurons 100, 154 and 25487 :  
`coordinates = H.load('coordinates', 'n', [100 154 25487]);`  
or only some time steps :  
`spikes = H.load('spikes', 't', 1:1000)`;  
Type `help FisH5.load` for more info.  
Generally, the following data is supported in FisH5.load and the viewers (see below):  

    'fluo'
    'baseline'
    'dff'
    'spikes'
    'spikesbin'

FisH5.load tries to load input data interpreted as a the absolute path to the dataset, if it fails, it tries hardcoded keyword, if it still fails it looks for a dataset with the input name.

### Viewers
FisH5 integrates some simple viewers with sliders.  
`H.displayMeanStack` : display the volumetric temporal mean with a z slider.  
`H.displayDynamicStack('dff')` : display the neurons filled with their DF/F over time with a z slider and a t slider. 'dff' can be replaced with any dataset.  
`H.displayTrace(1:10, 'spikes')` : plot the temporal trace of spikes for neurons 1 to 10. 'spikes' can be replaced with any dataset.  
`H.displaySignal('fluo', 'dff')` : shows the temporal mean with a z slider. You can click on any neuron on any slice to display the temporal trace of fluorescence and DF/F of the selected neuron. 'fluo' and 'dff' can be replaced with any dataset.  
`H.displayDynamicSignal('dff', 'spikes')` : combine `H.displayDynamicStack` and `H.displayTrace`. Also add a time cursor on the plots.  
`H.displayCorrelations('dff')` : display the temporal mean where you can click on any neuron to display the correlations with other neurons on the layer.