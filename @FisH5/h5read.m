function out = h5read(this, datasetname, n, t)
% out = FisH5.H5READ(datasetname, n, t)
% Wraps h5read specifying a subset of the dataset. Should be used through
% FisH5.load.
%
% INPUTS :
% ------
% datasetname : char, full path to the dataset.
% n : neurons indices to load or corresponding logical array.
% t : time steps indices to load or corresponding logical aray.

if contains(datasetname, 'pixel', 'IgnoreCase', true)
    out = h5read(this.filename, datasetname);
    return
end

if islogical(n)
    n = find(n);
end
if islogical(t)
    t = find(t);
end

n = sort(n);
t = sort(t);
start = [0, 0];
count = [0, 0];
if isempty(n)
    start(1) = 1;
    count(1) = Inf;
else
    start(1) = n(1);
    count(1) = numel(n(1):n(end));
end
if isempty(t)
    start(2) = 1;
    count(2) = Inf;
else
    start(2) = t(1);
    count(2) = numel(t(1):t(end));
end

% --- Load data
values = h5read(this.filename, datasetname, start, count);

% --- Restrict data to asked values
if isempty(n) && isempty(t)
    out = values;
elseif isempty(n)
    out = values(:, t - t(1) + 1);
elseif isempty(t)
    out = values(n - n(1) + 1, :);
else
    out = values(n - n(1) + 1, t - t(1) + 1);
end