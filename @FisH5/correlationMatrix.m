function R = correlationMatrix(this, data, varargin)
% R = FisH5.CORRELATIONSMATRIX(data)
% R = FisH5.CORRELATIONSMATRIX(_, 'n', 1:100)
% R = FisH5.CORRELATIONSMATRIX(_, 't', 1:100)
% Computes the corrected Pearson correlation coeffiecients for neurons on
% the same layers (not all the neurons because of memory limitations). The
% correlations matrix is corrected for correlated noise with first
% principal component.
%
% INPUTS :
% ------
% data : char, either 'dff', 'fluo', 'baseline', 'spikes' or 'spikes bin'.
% 'n', Value : neurons to correlate with each other, '' for all (huge
% memory consumption (> 30GB).
% 't', Value : time steps to take into account, '' for all.
% 
%
% RETURNS :
% -------
% R : nneurons x nneurons matrix containing correlation coeffecient
% corrected for correlated noise.

% --- Check input
p = inputParser;
p.addRequired('data', @(x) ischar(x)||isstring(x));
p.addParameter('n', '', @(x) ischar(x)||isstring(x)||isnumeric(x));
p.addParameter('t', '', @(x) ischar(x)||isstring(x)||isnumeric(x));
p.parse(data, varargin{:});
data = p.Results.data;
n = p.Results.n;
t = p.Results.t;

% --- Compute corrected Pearson's coefficient
R = corrcoef(this.load(data, 'n', n, 't', t)');
R(isnan(R)) = 0;
[v, lambda] = eig(R, 'vector');
lambda = real(lambda);
[maxeig, argmax] = max(lambda);
R = R - v(:, argmax)*v(:, argmax)'*maxeig;