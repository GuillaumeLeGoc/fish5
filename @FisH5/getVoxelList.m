function voxlist = getVoxelList(this, neurons_id)
% pixlist = FisH5.GETVOXELLIST(neurons_id).
% Returns the linear indices of neurons within the 3D stack.
%
% INPUTS :
% ------
% neurons_id : list of neurons' indices to locate.
%
% RETURNS :
% -------
% pixlist : cell, list of linear indices of neurons pixels.

%  --- Check version
if ~this.checkVersion(1)
    warning('File version not supported.');
    voxlist = [];
    return
end

% --- Get data
segm_mask = this.load('seg');

% --- Find neurons
R = regionprops(segm_mask, 'PixelIdxList');
voxlist = {R(neurons_id).PixelIdxList};