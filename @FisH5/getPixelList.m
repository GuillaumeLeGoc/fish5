function [pixlist, layer, id_on_layer] = getPixelList(this, neurons_id)
% [pixlist, layer, id_on_layer] = FisH5.GETPIXLIST(neurons_id).
% Finds pixels composing the input neurons index, the layer on wich it is
% and the index on the layer.
%
% INPUTS :
% ------
% neurons_id : list of neurons' indices to locate.
%
% RETURNS :
% -------
% pixlist : cell, each element is the pixel list in the temporal mean
% image.
% layer : cell, layers on which the neurons are.
% id_on_layer : neurons' indices within their respective layers.

% --- Get data
segm_mask = this.load('seg');

% --- Check version & use appropriate function
if ~this.checkVersion(1)
    coordinates = this.load('coordinates');
    dx = this.dx;
    [pixlist, layer, id_on_layer] = oldGetPix(logical(segm_mask), coordinates, neurons_id, dx);
else
    [pixlist, layer, id_on_layer] = newGetPix(segm_mask, neurons_id);
end

end

% -------------------------------------------------------------------------

function [pixlist, layer, id_on_layer] = oldGetPix(segm_mask, coordinates, neurons_id, dx)
% Find neurons pixels with regionprops and check centroids coordinates.

% Get layers
layers = unique(coordinates(:, 3), 'stable');

% Get neuron's coordinates
coord_neuron = coordinates(neurons_id, :);

% --- Locate neurons
layer = NaN(numel(neurons_id), 1);
id_on_layer = NaN(numel(neurons_id), 1);
pixlist = cell(numel(neurons_id), 1);
R = cell(numel(neurons_id), 1);
for id = 1:numel(neurons_id)
    
    % Get neuron's layer
    layer(id) = find(ismember(layers, coord_neuron(id, 3)), 1);
    
    % Get neurons pixels
    if isempty(R{layer(id)})
        R{layer(id)} = regionprops(segm_mask(:, :, layer(id)), 'PixelIdxList', 'Centroid');
    end
    r = R{layer(id)};
    pos = reshape([r(:).Centroid], [2 numel(r)])';
    
    id_on_layer(id) = find(ismembertol(pos, coord_neuron(id, [1 2])*1000/dx, 1e-6), 1);
    pixlist{id} = r(id_on_layer(id)).PixelIdxList';
end
end

function [pixlist, layer, id_on_layer] = newGetPix(segm_mask, neurons_id)
% Find neurons directly from labelled segmentation mask.

siz = size(segm_mask);

% Get neurons
R = regionprops(segm_mask, 'PixelIdxList');
R = {R(neurons_id).PixelIdxList};
layer = cellfun(@(x) unique(getZ(x, siz)), R);
pixlist = cellfun(@(x) getI(x, siz), R, 'UniformOutput', false);

if nargout > 2
    
    id_on_layer = NaN(numel(neurons_id), 1);
    for id = 1:numel(neurons_id)
        id_on_layer(id) = neurons_id(id) - sum(layer < layer(id));
    end
end
end

function out = getZ(pix, siz)
[~, ~, out] = ind2sub(siz, pix);    % find on which layer are the neurons
end

function out = getI(pix, siz)
[i, j, ~] = ind2sub(siz, pix);
out = sub2ind(siz(1:2), i, j);      % get linear indices within the plane
end