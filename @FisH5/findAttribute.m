function [values, locations, exactnames] = findAttribute(this, attrname, varargin)
% out = FisH5.FINDATTRIBUTES(attrname)
% out = FisH5.FINDATTRIBUTES(_, 'warn', false)
% Finds the path to the datasets with attributes containing attrname.
% It probably doesn't work for root attributes.
%
% INPUT :
% -----
% attrname : char, name of the attribute to find.
% 'warn', Value (optional) : boolean, turn on (default) or off the warnings
%
% RETURN :
% ------
% values : corresponding values of the attribute.
% locations : char or cell, patht to the dataset with the specified
% attribute.
% exactnames : exact name of the attribute.

% --- Check input
p = inputParser;
p.addRequired('attrname', @(x) ischar(x)||isstring(x));
p.addParameter('warn', true, @islogical);
p.parse(attrname, varargin{:});
attrname = p.Results.attrname;
warnings = p.Results.warn;

% --- Scan
groups = this.fileinfo;
append = '';
locations = cell(0, 1);
exactnames = cell(0, 1);
values = cell(0, 1);
while true
    
    groups = eval(['cat(1, groups(:)' append ')']);     % To check also root
    if isempty(groups)
        break;
    end
    
    names = cell(0, 1);
    for g = 1:length(groups)
        names = cat(1, names, cellstr(repmat(groups(g).Name, length(groups(g).Datasets), 1)));
    end
    
    % Check groups attributes
    for id = 1:length(groups)
        if ~isempty(groups(id).Attributes)
            attributesnames = {groups(id).Attributes(:).Name}';
            readvalues = {groups(id).Attributes(:).Value}';
            matched = contains(attributesnames, attrname, 'IgnoreCase', true);
            if any(matched)
                nmatch = sum(matched(:));
                locations = cat(1, locations, cellstr(repmat(strjoin([names(id) groups(id).Name], ''), nmatch, 1)));
                exactnames = cat(1, exactnames, attributesnames(matched));
                values = cat(1, values, readvalues(matched));
            end
        end
    end
        
    datasets = cat(1, groups(:).Datasets);              % Get all datasets
        
    for id = 1:length(datasets)
        
        if ~isempty(datasets(id).Attributes)
            attributesnames = {datasets(id).Attributes(:).Name}';
            readvalues = {datasets(id).Attributes(:).Value}';
            matched = contains(attributesnames, attrname, 'IgnoreCase', true);
            if any(matched)
                nmatch = sum(matched(:));
                locations = cat(1, locations, cellstr(repmat(strjoin([names(id) datasets(id).Name], ''), nmatch, 1)));
                exactnames = cat(1, exactnames, attributesnames(matched));
                values = cat(1, values, readvalues(matched));
            end
        end
    end
    
    append = '.Groups';
    
end

if isempty(locations)
    locations = [];
    exactnames = [];
    values = [];
    if warnings
        warning('Attribute not found.');
    end
elseif length(locations) > 1
    if warnings
        warning([num2str(length(locations)) ' attributes found.']);
    end
else
    locations = strjoin(locations, '');
    exactnames = strjoin(exactnames, '');
    values = values{1};
end