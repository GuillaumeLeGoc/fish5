function convoluted = convolve(this, data)
% data_convolved = FisH5.CONVOLVE(data)
% Convolution of input data with the GCaMP kernel found in
% InferenceParameters.
%
% INPUTS :
% ------
% data : array, n_neurons x n_times to convolve with GCaMP6 kernel.
% Typically, one wants to convolve spikes to recover denoised DF/F.
%
% RETURNS :
% -------
% convoluted : data * K

% --- Check input
p = inputParser;
p.addRequired('data', @isnumeric);
p.parse(data);

data = p.Results.data;

% --- Get data
eta = this.findAttribute('eta');
gamma = this.findAttribute('gamma');
delta = this.findAttribute('delta');

% --- Processing
convoluted = filter(1, [eta, -eta*(gamma + delta), eta*delta], data')';